variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "AWS_REGION" {
    default = "eu-west-3"
  
}

variable "AWS_AMI" {
    default = "ami-01b32e912c60acdfa"
  
}