resource "aws_security_group" "test-sg" {
     name = "test-sg"


    egress {
       from_port = 0
       to_port = 0
       protocol = "-1"
       cidr_blocks = [ "0.0.0.0/0" ]

    }
    ingress {
       from_port = 80
       to_port = 80
       protocol = "tcp"
       cidr_blocks = [ "0.0.0.0/0" ]

    }
  
}

resource "aws_instance" "myec2" {
    ami = var.AWS_AMI
    instance_type = "t2.micro"
    vpc_security_group_ids = [ aws_security_group.test-sg.id ]
    user_data = <<-EOF
       #!/bin/bash
       sudo apt update
       sudo apt install -y apache2
       sudo systemctl start apache2
       sudo systemctl enable apache2 
       sudo echo "<h1>Hello Devops</h1>" > /var/www/html/index.html

    EOF

    tags = {
         Name = "ec2-test"

    }
}

output "ip_public" {
    value = aws_instance.myec2.public_ip 
  
}